<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


<div class="container">
	<div class="row">
		<div class="col-md-9 content-holder">
			<div class="col-md-12">

				<?php
				if ( have_posts() ) while ( have_posts() ) : the_post();
					$news_title1 = get_the_title();
					$news_cont1 = get_the_excerpt();
					$news_thumb = get_post_meta($post->ID,'default_thumbnail',true);
					$Day = get_the_date('d');
					$Month = get_the_date('M');
					$Year = get_the_date('Y');

					if(!in_category('1')){
						?>

						<div class="category-news">

							<div class="category-news-right">

								<div class="category-news-right-title">
									<h4><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h4>
								</div>

								<div class="category-news-right-text">
									<p><?php
										if(strlen($news_cont1)>250){
											echo substr($news_cont1,0,250).'...';
										}else{
											echo $news_cont1;
										}
										?> <a href="<?php the_permalink();?>" title="<?php the_title();?>">Läs mer.</a></p>
								</div>

							</div>

							<div class="category-news-date">
								<p><?php the_date(); ?></p>
							</div>

							<div style="clear:both;"></div>

						</div><!-- .category-news -->

						<div style="clear:both;"></div>

						<div style="margin-top: 20px; margin-bottom: 20px; background: #ccc; height: 1px;"></div>

					<?php } ?>
				<?php endwhile; ?>


			</div>

		</div>



		<div class="col-md-3">
			<?php include('site_bar.php'); ?>

			<div class="nyheter">
				<?php
				if(is_active_sidebar('nyheter')){
					dynamic_sidebar('nyheter');
				} ?>
			</div>

		</div>



	</div>
</div>


<?php get_footer(); ?>
