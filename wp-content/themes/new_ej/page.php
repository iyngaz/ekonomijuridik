<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="container">
    <div class="row">
        <div class="row">
            <div class="col-md-9">
                <div class="content-holder">
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                    <?php
                    // Start the loop.
                    while (have_posts()) : the_post();

                        // Include the page content template.
                        the_content();

                    endwhile;
                    ?>
                </div>
            </div>

            <div class="col-md-3">
                <?php include('site_bar.php'); ?>
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>
