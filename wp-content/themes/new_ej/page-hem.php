<?php /* Template Name: Page Hem */
get_header(); ?>

    <div class="container-fluid lgihtgrey">
        <div class="container">
            <div class="row">
                <div class="col-md-12 steps-holder" style="text-align: center;">
                    <h2><?php echo ot_get_option('3_col_title'); ?></h2>
                    <ul class="steps">
                        <li>
                            <div class="img-holder"><i class="fa fa-book"></i></div>
                            <div class="text">
                                <h3><?php echo ot_get_option('3_col_1_title'); ?></h3>
                                <p><?php echo ot_get_option('3_col_1_text'); ?></p>
                            </div>
                        </li>
                        <li>
                            <div class="img-holder"><i class="fa fa-gift"></i></div>
                            <div class="text">
                                <h3><?php echo ot_get_option('3_col_2_title'); ?></h3>
                                <p><?php echo ot_get_option('3_col_2_text'); ?></p>
                            </div>
                        </li>
                        <li>
                            <div class="img-holder"><i class="fa fa-user-plus"></i></div>
                            <div class="text">
                                <h3><?php echo ot_get_option('3_col_3_title'); ?></h3>
                                <p><?php echo ot_get_option('3_col_3_text'); ?></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid home-content-box" style="padding:0;">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid selector">
        <div class="container selector-container">
            <div class="row">
                <div class="col-lg-12">
                    <h2><?php echo ot_get_option('selection_box_title'); ?></h2>
                    <div class="devider"></div>
                    <div class="input-group selection-form">
                        <?php echo do_shortcode("[wp_service_form]"); ?>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->

        </div>
    </div>


    <div class="container-fluid home-content-box">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <?php the_field('content_2'); ?>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid testimonials grey" style="padding-bottom: 20px;">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <h2>Detta säger våra kunder om oss</h2>
                </div>

                <?php wp_reset_query(); ?>
                <?php
                $testimonials = new WP_Query('category_name=testimonials&&posts_per_page=4order=asc');
                while ($testimonials->have_posts()) : $testimonials->the_post();
                    ?>


                    <div class="col-sm-5 col-md-6">
                        <div class="testimonial-box">
                            <div class="col-md-3 testimonial-box-img">
                                <img src="<?php the_field('avatar'); ?>">
                            </div>
                            <div class="col-md-9">
                                <h4><?php the_title(); ?></h4>
                        <span><?php
                            $rating = get_field('rating');
                            for ($x = 0; $x < $rating; $x++) { ?>
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/star.jpg">
                            <?php } ?></span>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>

                <?php endwhile; ?>

                <?php wp_reset_query(); ?>


            </div>
        </div>
    </div>


    <div class="container-fluid home-content-box">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <?php the_field('content_3'); ?>
                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>