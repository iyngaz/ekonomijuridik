<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<div class="container-fluid footer-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12 social-media">
                <ul>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="https://www.facebook.com/pages/Ekonomijuridikse/809543735779196"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>

                </ul>
            </div>

            <div class="col-md-12 footer-menu">
                <?php wp_nav_menu( array(
                    'menu' => 'footer-menu'
                ) );
                ?>
               <!-- <ul>
                    <li><a href="#"><Hem</a></li>
                    <li><a href="#">Få 3 offerter</li>
                    <li><a href="#">Välj ämne</li>
                    <li><a href="#">Nyheter</li>
                    <li><a href="#">Om</li>
                    <li><a href="#">Kontakt</li>
                </ul> -->
            </div>
            </div>
        </div>
    </div>

<div class="container-fluid footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 footer-box">
                <div class="footer-logo">
                <a href="<?php echo site_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo-footer.svg"></a>
                </div>
                <?php /*
                if(is_active_sidebar('footer-sidebar-1')){
                    dynamic_sidebar('footer-sidebar-1');
                } */ ?>
            </div>

            <div class="col-md-3 footer-box">
                <?php
                if(is_active_sidebar('footer-sidebar-2')){
                    dynamic_sidebar('footer-sidebar-2');
                } ?>
            </div>

            <div class="col-md-3 footer-box">
                <?php
                if(is_active_sidebar('footer-sidebar-3')){
                    dynamic_sidebar('footer-sidebar-3');
                } ?>
            </div>

            <div class="col-md-3 footer-box">
                <?php
                if(is_active_sidebar('footer-sidebar-4')){
                    dynamic_sidebar('footer-sidebar-4');
                } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 copyright-text">
                <p><?php echo ot_get_option('footer_text'); ?></p>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/js/nav.jquery.min.js"></script>
<script>
    $('.nav').nav();
</script>

<?php wp_footer(); ?>

</body>
</html>