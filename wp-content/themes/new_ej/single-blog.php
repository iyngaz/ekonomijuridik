<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div id="breadcrumbs">
  <?php if(function_exists('bcn_display')) { bcn_display(); }?>
</div>
<div class="row-auto cf">
  <div class="a-grid">
  <div class="a-col-pl-side-two no-back">
      <div id="sidebar">
      		<div class="widget"><h3>Nyheter</h3><ul class="enhanced-recent-posts">
            <?php wp_reset_query();?>
		<?php
			$testimonials = new WP_Query('category_name=blog&show_posts=10&order=asc');
			while($testimonials -> have_posts()) : $testimonials -> the_post();
        ?>
				<li>
					<a href="<?php echo get_permalink(); ?>"><?php the_title();?></a>
				</li>
      <?php endwhile; ?>
      <?php wp_reset_query();?>
</ul>
</div>
      </div>
    </div>

    <div class="a-col-pl-cont-two no-back">
  <main id="content" style="padding:0;">
      <h1 class="title">
        <?php the_title();?>
      </h1>
      <?php the_content();?>
  </main>
    </div>

    <div style="clear:both;"></div>
  </div>
</div>

<?php get_footer(); ?>
