<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>



  <div class="container">
    <div class="row">
      <div class="row">
        <div class="col-md-9">
          <div class="content-holder bog-box">
            <h1>
              <?php the_title(); ?>
            </h1>
            <?php
            // Start the loop.
            while (have_posts()) : the_post();

              // Include the page content template.
              the_content();

            endwhile;
            ?>
          </div>
        </div>

        <div class="col-md-3">
          <?php include('site_bar.php'); ?>
        </div>

      </div>
    </div>
  </div>

	
<?php get_footer(); ?>