<?php /* Template Name: Page - Blog */
get_header();?>



<div class="container">
  <div class="row">
    <div class="col-md-9 content-holder">
      <div class="col-md-12">
        <h1>
          <?php the_title(); ?>
        </h1>

        <p>
          <?php while ( have_posts() ) : the_post(); the_content(); ?>
          <?php endwhile; wp_reset_query(); ?>

        </p>
        <?php wp_reset_query();?>
        <?php
        $testimonials = new WP_Query('category_name=blog&order=asc');
        while($testimonials -> have_posts()) : $testimonials -> the_post();
          ?>
        <div class="bog-box">
          <h1 style="margin:0;"><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
<!--          <span style="font-size:12px; padding:0;"><?php the_date();?></span> -->
          <?php the_content( $more_link_text , $strip_teaser ); ?>
        </div>
          <div class="clear:both;"></div>

        <?php endwhile; ?>
        <?php wp_reset_query();?>

      </div>

    </div>



    <div class="col-md-3">
      <?php include('site_bar.php'); ?>

      <div class="nyheter">
        <?php
        if(is_active_sidebar('nyheter')){
          dynamic_sidebar('nyheter');
        } ?>
      </div>

    </div>



  </div>
</div>
















<?php /*




<div id="main">
  <div class="twocolumns">
    <div id="content">
      <div class="content-holder">
        <div class="intro-content inner-content">
          <h1>
            <?php the_title();?>
          </h1>
          <p>
            <?php while ( have_posts() ) : the_post(); the_content(); ?>
            <?php endwhile; wp_reset_query(); ?>

          </p>
			<?php wp_reset_query();?>
            <?php
				$testimonials = new WP_Query('category_name=blog&order=asc');
				while($testimonials -> have_posts()) : $testimonials -> the_post();
            ?>

           <h1 style="margin:0;"><a href="<?php the_permalink();?>"><?php the_title();?></a></h1>
           <span style="font-size:12px; padding:0;"><?php the_date();?></span>
           <?php the_content( $more_link_text , $strip_teaser ); ?>

            <div style="height:2px; border-bottom:1px solid #ccc; margin-bottom:27px;"></div>
		  <?php endwhile; ?>
          <?php wp_reset_query();?>
          </div>
        </div>
    </div>
    <div id="sidebar">
		<?php include('site_bar.php');?>
    </div>
  </div>
</div>

 */ ?>

<?php get_footer(); ?>
