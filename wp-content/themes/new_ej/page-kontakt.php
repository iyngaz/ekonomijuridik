<?php /* Template Name: Page - Kontakt */
get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-md-12 content-holder">
            <div class="col-md-12">
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                    <?php while (have_posts()) : the_post();
                        the_content(); ?>
                    <?php endwhile;
                    wp_reset_query(); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>
