<?php /* Template Name: Page - amnen */
get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-md-9 content-holder">
            <div class="col-md-12">
                    <h1>
                        <?php the_title(); ?>
                    </h1>
                    <?php while (have_posts()) : the_post();
                        the_content(); ?>
                    <?php endwhile;
                    wp_reset_query(); ?>
            </div>
            <div style="margin-top: 30px; margin-bottom: 30px;" class="col-md-12 main-form">
                <p>Få erbjudanden kostnadsfritt från upp till 3 revisorer / redovisningskonsulter</br> inom 36 timmar</p>
                <div class="list-b">
                    <ul>
                        <li>Revision</li>
                        <li>Bokföring</li>
                        <li>Lönehantering</li>
                    </ul>
                </div>
                <form id="contact_form" class="col-md-12">
                    <input type="hidden"  id="ej_channel_id" value="7">
                    <div class="form-group">
                        <input type="text" class="form-control" id="ej_name" placeholder="Namn" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="ej_phone" placeholder="Telefon" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="ej_mail" placeholder="E-post" required>
                    </div>
                    <div class="form-group">
                        <textarea id="ej_message" class="form-control" rows="6" placeholder="Meddelande" required></textarea>
                    </div>
                    <div class="col-md-12" style="text-align: right; padding-right: 0;">
                        <input type="submit" class="btn btn-default" value="SKICKA">
                    </div>
                </form>
            </div>



            <?php wp_reset_query(); ?>
            <?php
            $testimonials = new WP_Query('category_name=amnen&order=asc');
            while ($testimonials->have_posts()) : $testimonials->the_post();
            ?>
            <div class="col-md-6 amen-list">
                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title"><?php the_title(); ?></h4>
                        <p class="card-text"><?php the_content(); ?></p>
                    </div>
                    <?php the_field('links'); ?>

                  <!--  <ul class="list-group list-group-flush">
                        <li class="list-group-item">Cras justo odio</li>
                        <li class="list-group-item">Dapibus ac facilisis in</li>
                        <li class="list-group-item">Vestibulum at eros</li>
                    </ul> -->
                </div>
            </div>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>


        </div>



        <div class="col-md-3">
                <?php include('site_bar.php'); ?>
        </div>

    </div>
</div>

<?php get_footer(); ?>
