<?php /* Template Name: Page - fa 3 offerter */
get_header();?>


<div class="container">
  <div class="row">
    <div class="row">
      <div class="col-md-9">
        <div class="content-holder">
          <h1>
            <?php the_title(); ?>
          </h1>

          <div class="steps-holder" style="margin-top: 25px;">
            <ul class="steps">
              <li>
                <div class="img-holder"><i class="fa fa-book"></i></div>
                <div class="text">
                  <h2><?php the_field('1)_title');?></h2>
                  <p><?php the_field('1)_text');?></p>
                </div>
              </li>
              <li>
                <div class="img-holder"><i class="fa fa-gift"></i></div>
                <div class="text">
                  <h2><?php the_field('2)_title');?></h2>
                  <p><?php the_field('2)_text');?></p>
                </div>
              </li>
              <li>
                <div class="img-holder"><i class="fa fa-user-plus"></i></div>
                <div class="text">
                  <h2><?php the_field('3)_title');?></h2>
                  <p><?php the_field('3)_text');?></p>
                </div>
              </li>
            </ul>
          </div>
       <!--   <div class="form">
            <fieldset>
              <?php echo do_shortcode('[contact-form-7 id="51" title="kontakt Form"]');?>
            </fieldset>
          </div> -->


          <div style="margin-top: 30px; margin-bottom: 30px;" class="col-md-12 main-form">
            <p>Få erbjudanden kostnadsfritt från upp till 3 revisorer / redovisningskonsulter</br> inom 36 timmar</p>
            <div class="list-b">
              <ul>
                <li>Revision</li>
                <li>Bokföring</li>
                <li>Lönehantering</li>
              </ul>
            </div>
            <form id="contact_form" class="col-md-12">
              <input type="hidden"  id="ej_channel_id" value="7">
              <div class="form-group">
                <input type="text" class="form-control" id="ej_name" placeholder="Namn" required>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="ej_phone" placeholder="Telefon" required>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="ej_mail" placeholder="E-post" required>
              </div>
              <div class="form-group">
                <textarea id="ej_message" class="form-control" rows="6" placeholder="Meddelande" required></textarea>
              </div>
              <div class="col-md-12" style="text-align: right; padding-right: 0;">
                <input type="submit" class="btn btn-default" value="SKICKA">
              </div>
            </form>
          </div>

          
        </div>
      </div>

      <div class="col-md-3">
        <?php include('site_bar.php'); ?>
      </div>

    </div>
  </div>
</div>


<?php get_footer(); ?>
