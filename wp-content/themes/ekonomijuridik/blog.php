<?php
/**
 * Template Name: Page - Blog
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div id="breadcrumbs">
  <?php if(function_exists('bcn_display')) { bcn_display(); }?>
</div>
<div class="row-auto cf">
  <div class="a-grid">
    <div class="a-col-pl-cont-two no-back">
     	<?php wp_reset_query();?>
		<?php
			$testimonials = new WP_Query('category_name=blog&order=asc');
			while($testimonials -> have_posts()) : $testimonials -> the_post();
        ?>
      <div class="block post-feed-container cf">
        <h2><a href="<?php echo get_permalink(); ?>" rel="bookmark"><?php the_title();?></a></h2>
        <div class="meta-date"><?php the_date(); ?></div>
        <?php the_content();?>
        <div class="postmeta"> <span class="left"></span> </div>
      </div>
      <?php endwhile; ?>
      <?php wp_reset_query();?>
    </div>
    <div class="a-col-pl-side-two no-back">
      <div id="sidebar">
      		<div class="widget"><h3>Senaste</h3><ul class="enhanced-recent-posts">
            <?php wp_reset_query();?>
		<?php
			$testimonials = new WP_Query('category_name=blog&show_posts=10&order=asc');
			while($testimonials -> have_posts()) : $testimonials -> the_post();
        ?>
				<li>
					<a href="<?php echo get_permalink(); ?>"><?php the_title();?></a>
				</li>
      <?php endwhile; ?>
      <?php wp_reset_query();?>
</ul>
</div>
      </div>
    </div>
    <div style="clear:both;"></div>
  </div>
</div>
<?php get_footer(); ?>
