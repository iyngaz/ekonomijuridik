<div class="widget widget_text sideholder" id="text-13"> <strong style="font-size:28px;" class="title">Få 3 erbjudande kostnadsfritt</strong>
        <div class="inner-holder">
         <ul class="side-list">
            <li>Vi gör jobbet åt dig - Ta emot upp till 3 erbjudanden</li>
            <li>Vi har samarbeten över hela Sverige</li>
            <li>Tjänsten är gratis och dina 3 erbjudanden är helt utan förpliktelser</li>
          </ul>
          <a href="<?php echo site_url();?>/kontakt/" class="link">Be om en offert</a> </div>
      </div>

		<?php $map = get_field('display_map');
			if ($map == 'yes'){
				 include('map.php');
			} else { }
	 ?>

      <div class="widget widget_text " id="text-9">
        <h2>Omdömen</h2>
        <ul class="comments">
          <?php wp_reset_query();?>
          <?php
			$testimonials = new WP_Query('category_name=testimonials&posts_per_page=4order=asc');
			while($testimonials -> have_posts()) : $testimonials -> the_post();
          ?>
          <li>
            <header class="heading"> <img src="<?php the_field('avatar');?>" class="alignleft">
              <div class="heading-text">
                <h3>
                  <?php the_title();?>
                </h3>
                <div class="placeholder">
                  <?php
								$rating = get_field('rating'); 
                                for ($x = 0; $x < $rating; $x++) { ?>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/star.jpg">
                  <?php } ?>
                </div>
              </div>
            </header>
            <div class="text-area">
              <p><?php the_content();?></p>
            </div>
          </li>
          <?php endwhile; ?>
          <?php wp_reset_query();?>
        </ul>
      </div>
      
     