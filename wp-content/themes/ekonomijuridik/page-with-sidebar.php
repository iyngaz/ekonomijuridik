<?php /* Template Name: Page - Right Side Bar */
get_header();?>

<div id="main">
  <div class="twocolumns">
    <div id="content">
      <div class="content-holder">
        <div class="intro-content inner-content">
          <h1>
            <?php the_title();?>
          </h1>
          <p>
            <?php while ( have_posts() ) : the_post(); the_content(); ?>
            <?php endwhile; wp_reset_query(); ?>
            
          </p>
          
        </div>
      </div>
    </div>
    <div id="sidebar">
		<?php include('site_bar.php');?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
