<?php /* Template Name: Page - amnen */
get_header();?>

<div id="main">
  <div class="twocolumns">
    <div id="content">
      <div class="content-holder">
        <div class="intro-content inner-content">
          <h1>
            <?php the_title();?>
          </h1>
          <p>
            <?php while ( have_posts() ) : the_post(); the_content(); ?>
            <?php endwhile; wp_reset_query(); ?>
            
          </p>
           <ul class="subjects">
			<?php wp_reset_query();?>
            <?php
				$testimonials = new WP_Query('category_name=amnen&order=asc');
				while($testimonials -> have_posts()) : $testimonials -> the_post();
            ?>
            <li>
              <header class="header">
                <div class="heading-box">
                  <div><img class="alignright" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/<?php the_field('icon');?>"></div>
                  <h2><?php the_title();?></h2>
                </div>
                <p><?php the_content();?></p>
              </header>
                <?php the_field('links');?>
            </li>
		  <?php endwhile; ?>
          <?php wp_reset_query();?> 
            
          </ul>
        </div>
      </div>
    </div>
    <div id="sidebar">
		<?php include('site_bar.php');?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
