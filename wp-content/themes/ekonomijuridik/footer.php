<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package creativefocus
 */
?>

<footer id="footer">
  <div class="footer-b">
    <div class="holder">
      <div class="col-holder">
        <div class="col widget_nav_menu " id="nav_menu-1">
          <div class="open-close">
            <?php
                                    if(is_active_sidebar('footer-sidebar-1')){
                                    dynamic_sidebar('footer-sidebar-1');
                                    }
                                ?>
          </div>
        </div>
        <div class="col widget_nav_menu " id="nav_menu-2">
          <div class="open-close">
            <?php
                                    if(is_active_sidebar('footer-sidebar-2')){
                                    dynamic_sidebar('footer-sidebar-2');
                                    }
                                ?>
          </div>
        </div>
        <div class="col widget_nav_menu " id="nav_menu-3">
          <div class="open-close">
            <?php
				if(is_active_sidebar('footer-sidebar-3')){
				dynamic_sidebar('footer-sidebar-3');
				}
			?>
          </div>
        </div>
        <div class="col widget_text last" id="text-7">
          <div class="open-close">
            <?php
				if(is_active_sidebar('footer-sidebar-4')){
				dynamic_sidebar('footer-sidebar-4');
				}
			?>
          </div>
        </div>
      </div>
      <span class="footer-info"><?php echo ot_get_option( 'footer_text', $default ); ?></span> </div>
  </div>

	
	


</footer>
</div>
<?php wp_footer(); ?>
</body></html>