<?php /* Template Name: Page - partner */
get_header();?>

<div id="main">
  <div class="main-area">
    <header class="header-box">
      <h1>
        <?php while ( have_posts() ) : the_post(); the_content(); ?>
        <?php endwhile; wp_reset_query(); ?>
      </h1>
    </header>
    <ul>
      <li>
        <div class="holder">
          <p><img class="alignleft" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img20.png" alt=""></p>
          <div class="text">
            <h2>Kvalitetssäkrade uppdrag</h2>
            <p>Vår personal ringer alltid upp och kontrollerar samt verifierar uppdragen som kommer in.</br>För oss är det viktigt att våra partners känner att dem får <b>kvalité före kvantitet.</b></p>
          </div>
        </div>
      </li>
      <li>
        <div class="holder">
          <p><img class="alignleft" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img20.png" alt="image description"></p>
          <div class="text">
            <h2>Inga förpliktelser</h2>
            <p>De fleste av våra partners startar med ett gratiskonto utan förpliktelser. Här kan man se vilka uppdrag Ekonomijuridik.se får in som vi kan erbjuda dig. <b>Skaffa ditt gratiskonto idag</b></p>
          </div>
        </div>
      </li>
      <li>
        <div class="holder">
          <p><img class="alignleft" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img20.png" alt=""></p>
          <div class="text">
            <h2>Fast kontaktperson</h2>
            <p>Du får direktnummer och mail till din fasta kontaktperson. Ringe eller skriv, om det är något - <b>vi är tillgängliga hela dagen.</b></p>
          </div>
        </div>
      </li>
      <li>
        <div class="holder">
          <p><img class="alignleft" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img20.png" alt=""></p>
          <div class="text">
            <h2>Kostar inget, när du inte kan ta emot kunder</h2>
            <p>Vi har förståelse för att våra våra klienter har fullt upp vissa perioder, <b>därför kostar vår tjänst ingenting när du inte använder den.</b></p>
          </div>
        </div>
      </li>
    </ul>
    <div class="blocks-area">
      <section class="info-block"><img class="alignright" src="<?php the_field('right_image');?>" alt="">
        <p></p>
        <div class="info-text">
          <?php the_field('box_content');?>
        </div>
      </section>
    </div>
  </div>
  <div class="main-container">
    <?php echo do_shortcode('[contact-form-7 id="48" title="Contact Form - Partner"]');?> </div>
  <div class="comments-area">
    <h2>Detta säger våra användare om oss</h2>
    <ul>
      <?php wp_reset_query();?>
      <?php
			$testimonials = new WP_Query('category_name=testimonials&&posts_per_page=4order=asc');
			while($testimonials -> have_posts()) : $testimonials -> the_post();
        ?>
      <li> <img src="<?php the_field('avatar');?>" class="alignleft">
        <div class="commet-text">
          <header class="header">
            <h3>
              <?php the_title();?>
            </h3>
            <div class="placeholder">
              <?php
								$rating = get_field('rating'); 
                                for ($x = 0; $x < $rating; $x++) { ?>
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/star.jpg">
              <?php } ?>
            </div>
          </header>
          <div class="text">
            <p>
              <?php the_content();?>
            </p>
          </div>
        </div>
      </li>
      <?php endwhile; ?>
      <?php wp_reset_query();?>
    </ul>
  </div>
</div>
<?php get_footer(); ?>
