<link rel="stylesheet" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery-jvectormap-2.0.4.css"/>
<link rel="stylesheet" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery-ui-1.8.22.custom.css"/>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-jvectormap-2.0.4.min.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/tabs.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-jvectormap-se-mill.js"></script>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-jvectormap-se-merc.js"></script>
</head><script>
			
  $(function(){
    $(".tabs").tabs("div.panes > div", {
      onClick: function(e, index){
        var cont = $('div.panes .jvectormap:nth('+index+')');
		var codel;	

        if (!cont.children('.jvectormap-container').data('mapObject')) {
          $('.jvectormap-'+cont.attr('data-projection')).vectorMap({
            map: cont.attr('data-name'),
           regionsSelectable: true, regionStyle: { initial: { fill: "#5d91bf", "fill-opacity": 1, stroke: "#6FB4FF", "stroke-width": 1 },
		   selected: { fill: "#5d91bf" }},
		   backgroundColor: "#ffffff", zoomButtons: false, zoomOnScroll: false, 
		   onRegionOver : function(event, code){document.body.style.cursor = "pointer";
				document.getElementById(code).style.fontWeight = "bold";
		   }, 
		   onRegionOut: function(element, code, region) { document.body.style.cursor = "default";
		   		document.getElementById(code).style.fontWeight = "normal";
		   },
		   
		   onRegionClick : function(event, code){
				  var mapLinks = '.container-'+codel;
					$(mapLinks).each(function() {
						$(this).addClass('hide');
					}); // Check last activated div and hide it
				var mapLinks = '.container-all';
					$(mapLinks).each(function() {
					$(this).addClass('hide');
				});	 // display the click div
				var mapLinks = '.menu-item';
					$(mapLinks).each(function() {
					$(this).addClass('tf-open');
				});	// open the tree view list
				  var mapLinks = '.container-'+code;
						$(mapLinks).each(function() {
						$(this).removeClass('hide');
						codel = code;
					});	// hide the div and send the code to a variable	  
			   }, 
          });
	
		// tf-open 
        }
      }
    });
	
  });
  
  
</script>

<ul class="tabs">
</ul>
<div class="panes">
  <div>
    <div id="world-map" style="margin:0 auto; width: 230px; height: 440px;" class="jvectormap jvectormap-mill" data-projection="mill" data-name="se_mill"></div>
  </div>
</div>
<style>
	ul#my-tree	{margin:0; padding:10px 5px; color:#666;}
	ul#my-tree	li		{margin:8px 0;}
	div.desc	{margin:20px 0; color:#aaa; font-size:11px; text-align:left;}
</style>
<style>
 	.hide { display:none; z-index:999999;}
 </style>
<link type="text/css" rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/jquery.treefilter.css">
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.treefilter.js"></script> 
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/TreeListFilter.js"></script>
<div class="citylist-grid">
  <div id="citylistall" class="container-all"> 
    <script>
$(function() {

	var tree = new treefilter($("#root_tree"), {
		searcher : $("input#my-searchall"),
		multiselect : true
	});
});
</script>
<?php
   function the_map_link() {
  		echo $slugg = the_parent_slug();
	
	if ($slugg == NULL){
		$sluggnew = the_permalink(); // get the page link
		return $sluggnew;
	}else {
			$siteurl = get_site_url(); // get site url
			$slash = '/';
			$sluggnew = $siteurl.$slash.$slugg.$slash; // create the final link
			return $sluggnew;
	}
} ?>
    <input type="search" id="my-searchall" class="search-field" placeholder="sök">
    <ul id="root_tree" class="nav-menu tf-tree">
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-O"><a>Västra Götaland</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>goteborg/">Göteborg</a></div>
          </li>
        </ul>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-N"><a>Halland</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>halmstad/">Halmstad</a></div>
          </li>
        </ul>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-M"><a>Skåne</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>lund/">Lund</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>helsingborg/">Helsingborg</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>malmo/">Malmö</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>kristianstad/">Kristianstad</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>trelleborg/">Trelleborg</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>hassleholm/">Hässleholm</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>helsingborg/">Helsingborg</a></div>
          </li>
        </ul>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-K"><a>Blekinge</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>karlskrona/">Karlskrona</a></div>
          </li>
        </ul>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-I"><a href="<?php echo the_map_link();?>gotland/">Gotland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-H"><a>Kalmar</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-G"><a>Kronoberg</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-F"><a>Jönköping</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-E"><a>Östergötland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-D"><a>Södermanland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-C"><a>Uppsala</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-AC"><a>Västerbotten</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-Z"><a>Jämtland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-Y"><a>Västernorrland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-X"><a>Gävleborg</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-W"><a>Dalarna</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-AB"><a href="<?php echo the_map_link();?>stockholm/">Stockholm</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-U"><a>Västmanland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-T"><a>Orebro</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-S"><a>Värmland</a></div>
      </li>
      <li class="menu-item" style="padding-left: 20px;">
        <div id="SE-BD"><a>Norrbotten</a></div>
      </li>
    </ul>
  </div>
  <div id="citylist_vastra_gotaland" class="container-SE-O hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_vastra_gotaland"), { searcher : $("input#my-search-citylist_vastra_gotaland"), multiselect : true }); }); </script>
    <input type="search" id="my-search-citylist_vastra_gotaland" class="search-field" placeholder="sök">
    <ul id="root_tree_vastra_gotaland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Västra Götaland</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>goteborg/">Göteborg</a></div>
          </li>
        </ul>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_halland" class="container-SE-N hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_halland"), { searcher : $("input#my-search-citylist_halland"), multiselect : true }); }); </script>
    <input type="search" id="my-search-citylist_halland" class="search-field" placeholder="sök">
    <ul id="root_tree_halland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Västra Götaland</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>goteborg/">Göteborg</a></div>
          </li>
        </ul>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_skane" class="container-SE-M hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_skane"), { searcher : $("input#my-search-citylist_skane"), multiselect : true }); }); </script>
    <input type="search" id="my-search-citylist_skane" class="search-field" placeholder="sök">
    <ul id="root_tree_skane" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Skåne</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>lund/">Lund</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>helsingborg/">Helsingborg</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>malmo/">Malmö</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>kristianstad/">Kristianstad</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>trelleborg/">Trelleborg</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>hassleholm/">Hässleholm</a></div>
          </li>
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>helsingborg/">Helsingborg</a></div>
          </li>
        </ul>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_blekinge" class="container-SE-K hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_blekinge"), { searcher : $("input#my-search-citylist_blekinge"), multiselect : true }); }); </script>
    <input type="search" id="my-search-citylist_blekinge" class="search-field" placeholder="sök">
    <ul id="root_tree_blekinge" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Blekinge</a></div>
        <ul class="sub-menu">
          <li style="padding-left: 20px;">
            <div><a href="<?php echo the_map_link();?>karlskrona/">Karlskrona</a></div>
          </li>
        </ul>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_gotland" class="container-SE-I hide"> 
    <script> $(function() { var tree = new treefilter($("#root_tree_gotland"), { searcher : $("input#my-search-citylist_gotland"), multiselect : true }); }); </script>
    <input type="search" id="my-search-citylist_gotland" class="search-field" placeholder="sök">
    <ul id="root_tree_gotland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a href="<?php echo the_map_link();?>gotland/">Gotland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_kalmar" class="container-SE-H hide"> 
    <script> $(function() { var tree = new treefilter($("#root_tree_kalmar"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_kalmar" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Kalmar</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_kronoberg" class="container-SE-G hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_kronoberg"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_kronoberg" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Kronoberg</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_jonkoping" class="container-SE-F hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_jonkoping"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_jonkoping" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Jönköping</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_ostergotland" class="container-SE-E hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_ostergotland"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_ostergotland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Östergötland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_sodermanland" class="container-SE-D hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_sodermanland"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_sodermanland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Södermanland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_uppsala" class="container-SE-C hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_uppsala"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_uppsala" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Uppsala</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_vasterbotten" class="container-SE-AC hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_vasterbotten"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_vasterbotten" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Västerbotten</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_jamtland" class="container-SE-Z hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_jamtland"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_jamtland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Jämtland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_vasternorrland" class="container-SE-Y hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_vasternorrland"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_vasternorrland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Västernorrland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_gavleborg" class="container-SE-X hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_gavleborg"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_gavleborg" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Gävleborg</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_dalarna" class="container-SE-W hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_dalarna"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_dalarna" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Dalarna</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_stockholm" class="container-SE-AB hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_stockholm"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_stockholm" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a href="<?php echo the_map_link();?>stockholm/">Stockholm</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_vastmanland" class="container-SE-U hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_vastmanland"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_vastmanland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Västmanland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_orebro" class="container-SE-T hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_orebro"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_orebro" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Orebro</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_varmland" class="container-SE-S hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_varmland"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_varmland" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Värmland</a></div>
    </ul>
  </div>
  <!-- end -->
  
  <div id="citylist_norrbotten" class="container-SE-BD hide" > 
    <script> $(function() { var tree = new treefilter($("#root_tree_norrbotten"), { searcher : $("input#my-search"), multiselect : true }); }); </script>
    <input type="search" id="my-search" class="search-field" placeholder="sök">
    <ul id="root_tree_norrbotten" class="nav-menu tf-tree">
      <li class="menu-item tf-child-true tf-open" style="padding-left: 20px;">
        <div><a>Norrbotten</a></div>
    </ul>
  </div>
  <!-- end --> 
  
</div>
<!-- citylist-grid -->