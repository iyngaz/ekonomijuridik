<?php /* Template Name: Page - Affiliate */
get_header();?>

<div id="main">
  <div class="twocolumns">
    <div id="content">
      <div class="content-holder">
        <div class="intro-content inner-content">
          <h1>
            <?php the_title();?>
          </h1>
          <p>
            <?php while ( have_posts() ) : the_post(); the_content(); ?>
            <?php endwhile; wp_reset_query(); ?>
            
          </p>
         
        </div>
      </div>
       <div class="three-col">
         	<div class="numberr"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/1.png"/></div>
            <h2><?php the_field('box_1_title');?></h2>
         	<p><?php the_field('box_1_description');?></p>
         </div><!-- three-col -->
         
         <div class="three-col">
         	<div class="numberr"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/2.png"/></div>
            <h2><?php the_field('box_2_title');?></h2>
         	<p><?php the_field('box_2_description');?></p>
         </div><!-- three-col -->
         
         <div class="three-col">
         	<div class="numberr"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/3.png"/></div>
            <h2><?php the_field('box_3_title');?></h2>
         	<p><?php the_field('box_3_description');?></p>
         </div><!-- three-col -->
    </div>
    <div id="content">
      <div class="content-holder">
      
    <div class="two-col">
    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/badges/1.png"/>
    </div><!-- two-col -->
    <div class="two-col">
    	<textarea style="font-size: 14px; padding:10px; height: 80px; width: 95%; float: right;" readonly="readonly"><a href="http://ekonomijuridik.se/"><img src="http://ekonomijuridik.se/wp-content/themes/twentyfifteen/images/badges/1.png" alt="Vi får kunder via Ekonomijuridik.se"></a></textarea>
    </div><!-- two-col -->
    
	<div style="clear:both; height:30px;"></div>
    <div class="two-col">
    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/badges/2.png"/>
    </div><!-- two-col -->
    <div class="two-col">
    	<textarea style="font-size: 14px; padding:10px; height: 80px; width: 95%; float: right;" readonly="readonly"><a href="http://ekonomijuridik.se/"><img src="http://ekonomijuridik.se/wp-content/themes/twentyfifteen/images/badges/2.png" alt="Vi får kunder via Ekonomijuridik.se"></a></textarea>
    </div><!-- two-col -->
    
    
	<div style="clear:both; height:30px;"></div>
    <div class="two-col">
    	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/badges/3.png"/>
    </div><!-- two-col -->
    <div class="two-col">
    	<textarea style="font-size: 14px; padding:10px; height: 80px; width: 95%; float: right;" readonly="readonly"><a href="http://ekonomijuridik.se/"><img src="http://ekonomijuridik.se/wp-content/themes/twentyfifteen/images/badges/3.png" alt="Vi får kunder via Ekonomijuridik.se"></a></textarea>
    </div><!-- two-col -->
    
    
	<div style="clear:both; height:30px;"></div>
    <div class="two-col" style="text-align:center;">
    	<img style="text-align:center;" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/badges/4.png"/>
    </div><!-- two-col -->
    <div class="two-col">
    	<textarea style="font-size: 14px; padding:10px; height: 80px; width: 95%; float: right;" readonly="readonly"><a href="http://ekonomijuridik.se/"><img src="http://ekonomijuridik.se/wp-content/themes/twentyfifteen/images/badges/4.png" alt="Vi får kunder via Ekonomijuridik.se"></a></textarea>
    </div><!-- two-col -->
    
    </div>
    </div>
    
    <div id="sidebar" style="top:-544px !important;">
		<?php include('site_bar.php');?>
    </div>
  </div>
</div>
<?php get_footer(); ?>