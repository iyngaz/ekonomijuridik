<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div id="breadcrumbs">
  <?php if(function_exists('bcn_display')) { bcn_display(); }?>
</div>

<div class="row-auto cf">
  <main id="content">
    <article class="main-content cf">
      <h1 class="title">
        <?php the_title();?>
      </h1>
      <?php the_content();?>
    </article>
  </main>
</div>

<?php get_footer(); ?>
