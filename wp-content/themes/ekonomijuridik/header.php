<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package creativefocus
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<link media="all" rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/all.css">
<link rel="stylesheet" type="text/css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/fancybox.css"/>
<link rel="stylesheet" id="jufforms-css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/forms.css" type="text/css" media="all">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png" />

<link rel="stylesheet" type="text/css" media="all" href="<?php echo esc_url( get_template_directory_uri() ); ?>/style.css"/>
<link rel='stylesheet' id='font-awesome-css'  href='<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-awesome.min.css' type='text/css' media='all' />
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/jquery.main.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/css3-mediaqueries.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/js-site.js"></script> <!-- responsive menu script -->


<?php wp_head(); ?>





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>


<script>
	$(document).ready(function() {
$("#call_me").click(function() {
	var date = $("#date").val();
	var location = $("#location").val();
		var name = $("#name").val();
		var telefon = $("#telefon").val();
		
$("#returnmessage").empty(); // To empty previous error/success message.


// Returns successful data submission message when the entered information is stored in database.
if (name =="" || telefon =="" ){
	alert ('Var god och fyll i ditt namn och telefonnummer. Klicka sedan på ring mig så ringer vi dig inom kort.');
}
else {
$.post("http://ekonomijuridik.se/frm/email.php", {
	date: date,
	location: location,
name: name,
telefon: telefon


}, function(data) {
	
//$("#returnmessage").append(data); // Append returned message to message paragraph.
	document.getElementById("call_window").style.display = "none";
	document.getElementById("success_call").style.display = "block";
});
}
});

});
</script>



<script type="text/javascript">
$(document).ready(function(){ 

	var _scroll = true, _timer = false, _floatbox = $("#contact_form"),  _floatbox_opener = $(".closePopUpBtnSmall") ; _floatbox_opener = $(".contact-opener") ;
	_floatbox.css("right", "-271px"); //initial contact form position
	
	//Contact form Opener button
	_floatbox_opener.click(function(){
		if (_floatbox.hasClass('visiable')){
            _floatbox.animate({"right":"-271px"}, {duration: 300}).removeClass('visiable');
        }else{
           _floatbox.animate({"right":"0px"},  {duration: 300}).addClass('visiable');
        }
	});
	
	var _scroll = true, _timer = false, _floatbox = $("#contact_form"), _floatbox_opener = $(".closePopUpBtnSmall") ;
	//Contact form Opener button
	_floatbox_opener.click(function(){
		if (_floatbox.hasClass('visiable')){
            _floatbox.animate({"right":"-271px"}, {duration: 300}).removeClass('visiable');
        }else{
           _floatbox.animate({"right":"0px"},  {duration: 300}).addClass('visiable');
        }
	});
	
	var _scroll = true, _timer = false, _floatbox = $("#contact_form"), _floatbox_opener = $(".close") ;
	//Contact form Opener button
	_floatbox_opener.click(function(){
		if (_floatbox.hasClass('visiable')){
            _floatbox.animate({"right":"-271px"}, {duration: 300}).removeClass('visiable');
        }else{
           _floatbox.animate({"right":"0px"},  {duration: 300}).addClass('visiable');
        }
	});

	
	
	
	//Effect on Scroll
	$(window).scroll(function(){
		if(_scroll){
			_floatbox.animate({"top": "30%"},{duration: 300});
			_scroll = false;
		}
		if(_timer !== false){ clearTimeout(_timer); }           
			_timer = setTimeout(function(){_scroll = true; 
			_floatbox.animate({"top": "30%"},{easing: "linear"}, {duration: 500});}, 400); 
	});
	
	
});
</script>


<!-- begin SnapEngage code -->
<script type="text/javascript">
  (function() {
    var se = document.createElement('script'); se.type = 'text/javascript'; se.async = true;
    se.src = '//storage.googleapis.com/code.snapengage.com/js/8821980d-47e0-45ae-b24d-43b169d99214.js';
    var done = false;
    se.onload = se.onreadystatechange = function() {
      if (!done&&(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {
        done = true;
        SnapEngage.setTitle("Kontakta oss");
        /* Place your SnapEngage JS API code below */
        /* SnapEngage.allowChatSound(true); Example JS API: Enable sounds for Visitors. */
      }
    };
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);
  })();
</script>
<!-- end SnapEngage code -->



<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='<?php echo get_template_directory_uri(); ?>/js/jquery-migrate.min.js?ver=1.2.1'></script>

<link rel='shortlink' href='<?php echo site_url();?>/' />


<!-- contact form start -->
<div class="floating-form" id="contact_form">
  <div class="contact-opener">Kontakta oss</div>

  
  <div id="support-slider-main"> <a href="javascript:loopia.supersupport.hide();" class="closePopUpBtnSmall"><img src="http://www.xn--trdfllning24-hcbc.se/images/close-window.png"></a>

    <div id="support-slider-start" style="display: block;">
      <h3>Hur kan vi hjälpa dig?</h3>
      <p>Välj något av följande sätt för att komma i kontakt med oss.</p>
    </div>
    <div class="clear">&nbsp;</div>
    
    <div class="support-slider-type"> 
        <a class="email-status-open" href="mailto:info@ekonomijuridik.se">
            <img src="http://www.xn--trdfllning24-hcbc.se/images/ss-email.png">
            <span>info@ekonomijuridik.se</span>
            <span id="email_status">&nbsp;</span>
        </a> 
    </div>
   



    
    <div class="support-slider-type"> 
        <a class="close" href="#" onclick="return SnapEngage.startLink();">
            <img src="http://www.xn--trdfllning24-hcbc.se/images/ss-chat.png"> 
            <span>Chatta med oss</span><span id="chat_status"> 
            <img style="width:20px;" src="https://www.snapengage.com/statusImage?w=8821980d-47e0-45ae-b24d-43b169d99214" alt="Contact us" border="0"></span> 
        </a> 
    </div>
    
    
      <?php 
date_default_timezone_set('Europe/Stockholm');

$time = date('G');
$status;
if( ($time >= 8 && $time < 20) ){
$status = 'open';
$link = 'href="tel://+4620910470"';
}
else {
	$status = 'closed';
	$link = '';
}

?>
    <?php 
$day = strftime("%A");
$time = date('G');
$status;

if( ($time >= 8 && $time < 17 && $day != 'Sunday' && $day != 'Saturday') ){
$status2 = 'open';
$link2 = '';
}
else {
	$status2 = 'closed';
	$link2 = '1';
}

?>
    <div id="call<?php echo $link2;?>" class="support-slider-type"> 
        <a class="callme-status-<?php echo $status2;?>">
            <img src="http://www.xn--trdfllning24-hcbc.se/images/ss-phone_in.png"> 
            <span>Vi ringer dig</span>
            <span id="callme_status">&nbsp;</span> 
        </a>        
    </div>
    <div class="support-slider-type"> 
    	<a <?php echo $link;?> class="callme-status-<?php echo $status;?>">
        <img src="http://www.xn--trdfllning24-hcbc.se/images/ss-phone.png">
        <span>020 - 910 470</span> 
        <span id="callme_status">&nbsp;</span> 
        </a>
    </div>
  </div>





<!-- callme -->
<div id="callme" style="display:none;">
<div id="support-slider-main">
                <a href="javascript:loopia.supersupport.hide();" class="closePopUpBtnSmall"><img src="http://www.xn--trdfllning24-hcbc.se/images/close-window.png"></a>

				<div id="call_window">
               <div id="support-slider-callback" style="">
                    <h3>Vi ringer dig</h3>
                    <p>Ange ditt telefonnummer så ringer någon av våra medarbetare upp dig snarast möjligt.</p>
					
<?php 

date_default_timezone_set('Europe/Stockholm');
$date = date('Y-m-d h:i', time());

?>
                    
                    
                    
                    <form id="form">
                        <input type="hidden" name="ajax_action" value="call_me">
                        <input value="<?php echo $date;?>" onfocus="this.select()"  type="hidden" id="date" name="date">
                        <input value="<?php echo $location;?>" onfocus="this.select()"  type="hidden" id="date" name="location">
                        
                        <input placeholder="Namn*" type="text" id="name" name="name" required>
                        <input placeholder="Telefonnummer*" id="telefon" type="text" name="telefon" required>
                        <p class="no-margin">
                          <!--  <a id="call_me" title="Ring mig" class="btn ctaBtn">Ring mig</a> -->
                            <input type="button" id="call_me" value="Ring mig"/>
                            <a id="main_window" class="support-slider-back">« Tillbaka</a>
                        </p>
                    </form>
                </div><!-- /end #support-slider-start -->
                </div>
                
                <div id="success_call" style="display:none;">
                <div id="support-slider-callback" style="">
                    <p>Tack, vi ringer dig inom kort.<br/></p>
                    <a id="main_window2" class="support-slider-back">« Tillbaka</a>
                </div><!-- /end #support-slider-start -->
                </div><!-- success_call -->
  
</div>

</div><!-- callme -->



</div>

<script type="text/javascript">
	$(document).ready(function() {

  $("#call").click(function () {
		document.getElementById("support-slider-main").style.display = "none";
		document.getElementById("callme").style.display = "block";
		
  });
  
  $("#main_window").click(function () {
		document.getElementById("support-slider-main").style.display = "block";
		document.getElementById("callme").style.display = "none";
  });
  
  $("#main_window2").click(function () {
		document.getElementById("support-slider-main").style.display = "block";
		document.getElementById("callme").style.display = "none";
		document.getElementById("success_call").style.display = "none";
		document.getElementById("call_window").style.display = "block";
  });

});
</script>



</head>





<body <?php body_class(); ?>>


<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NLH263"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLH263');</script>
<!-- End Google Tag Manager -->

<div id="wrapper">
<?php if(is_front_page()){ ?>
<div class="container home">
<img width="1200" height="541" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img111.jpg" class="attachment-homethumbnail wp-post-image"/>
<div class="holder">
<header id="header"> <strong class="logo"><a href="<?php echo site_url();?>" title="ekonomijuridik"></a></strong>
  <nav id="nav">
    <ul class="slide">
      <?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
    </ul>
  </nav>
<!--  <div class="phonenumber">
  	<i class="fa fa-phone"> <span><a href="tel://+46104958122">020 - 910 470</a></span></i>
  </div><!-- phonenumber -->
</header>
<?php }else { ?>
<div class="container">
  <div class="holder">
    <header id="header"> <strong class="logo"><a href="<?php echo site_url();?>"></a></strong>
      <nav id="nav">
        <ul class="slide">
          <?php wp_nav_menu( array('menu' => 'Main Menu' )); ?>
        </ul>
      </nav>
<!--  <div class="phonenumber">
  	<i class="fa fa-phone"> <span><a href="tel://+46104958122">020 - 910 470</a></span></i>
  </div><!-- phonenumber -->
    </header>
  </div>
</div>
<?php }?>
