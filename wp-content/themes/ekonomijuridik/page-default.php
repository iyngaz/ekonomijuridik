<?php
/**
 * Template Name: Page - Default
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div id="main">
  <div class="main-outer">
    <div id="content" style="width:100%">
      <div class="content-holder">
        <h1>
          <?php the_title();?>
        </h1>
        <?php while ( have_posts() ) : the_post(); the_content(); ?>
        <?php endwhile; wp_reset_query(); ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>