<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>


<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php
		// get Page name
		$page_id = get_the_id();
    ?>
	<?php 
		switch ($page_id){												
		case "14":
		include (TEMPLATEPATH . '/blog.php');
		break;
    ?>
    
	<?php		
		case "16";
		include (TEMPLATEPATH . '/contact.php');
		break;
    ?>
        
	<?php 	
    default:
    include (TEMPLATEPATH . '/default-page.php');
    } 
    ?>
<?php endwhile; else: ?>

<p>Sorry, no posts matched your criteria.</p>
<?php endif; ?>
<?php get_footer(); ?>
