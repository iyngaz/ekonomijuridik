<?php /* Template Name: Page - Kontakt */
get_header();?>

<div id="main">
  <div class="main-outer">
    <div class="outer-holder">
      <div class="steps-holder">
        <ul class="steps">
          <li>
            <div class="img-holder"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img2.png" alt="image description"></div>
            <div class="text">
              <h2><?php the_field('1)_title');?></h2>
              <p><?php the_field('1)_text');?></p>
            </div>
          </li>
          <li>
            <div class="img-holder"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img3.png" alt="image description"></div>
            <div class="text">
              <h2><?php the_field('2)_title');?></h2>
              <p><?php the_field('2)_text');?></p>
            </div>
          </li>
          <li>
            <div class="img-holder"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img24.png" alt="image description"></div>
            <div class="text">
              <h2><?php the_field('3)_title');?></h2>
              <p><?php the_field('3)_text');?></p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="form">
      <fieldset>
        <?php echo do_shortcode('[contact-form-7 id="51" title="kontakt Form"]');?>
      </fieldset>
    </div>
  </div>
</div>
<?php get_footer(); ?>
