<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package creativefocus
 */
get_header(); ?>

<!--

<script>
$(document).ready(function(){
 $(window).scroll(function(){
  // get the height of #wrap
  var h = $('#wrapper').height();
  var y = $(window).scrollTop();
  if( y > (h*.25) && y < (h*.75) ){
   // if we are show keyboardTips
   $("#pop-girl").fadeIn("slow");

  } else {
   $('#pop-girl').fadeOut('slow');
  }
 });
})
</script>

<div id="pop-girl">
    <div class="pop-image">
        <img src="<?php echo ot_get_option( 'image', $default ); ?>"/>
    </div>
    <div class="pop-text">
        <?php echo ot_get_option( 'message', $default ); ?>
    </div>
</div>

-->

<div class="caption">
  <h1><span>Få 3 kostnadsfria offerter från en revisor, redovisningsbyrå eller redovisningskonsult
    </span> </h1>
 <div class="btn-holder"><!-- <a href="#" class="link lightbox iframe">Se film</a> --> <a href="<?php echo site_url();?>/kontakt/" class="link link-offer">Begär offert</a> <span class="txt-gratis">gratis</span> </div>
</div>
</div>
</div>
<div id="main">
  <div class="main-block">
    <div class="steps-holder">
      <h2>Hur vår tjänst fungerar</h2>
      <ul class="steps">
        <li>
          <div class="img-holder"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img2.png" alt="image description"></div>
          <div class="text">
            <h2>Beskrivning</h2>
            <p>Skriv en rad eller två om vad det är du vill ha hjälp med.</p>
          </div>
        </li>
        <li>
          <div class="img-holder"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img3.png" alt="image description"></div>
          <div class="text">
            <h2>Få 3 erbjudanden</h2>
            <p>Upp till 3 advokater eller revisorer kommer att skicka erbjudanden och pris.</p>
          </div>
        </li>
        <li>
          <div class="img-holder"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img4.png" alt="image description"></div>
          <div class="text">
            <h2>Välj själv</h2>
            <p>Välj det erbjudande som tilltalar dig mest.</p>
          </div>
        </li>
      </ul>

    	
        <div class="three-boxes">
        

</div>
    </div>

  <!--  <div class="steps-holder quicklinks">
      <div class="contains">
      









</li>
        </ul>
      </div>
    </div> -->
    <div style="clear:both;"></div>
    
        
        <ul>
      <?php wp_reset_query();?>
      <?php
            $testimonials = new WP_Query('category_name=front-3-box&&posts_per_page=3order=asc');
            while($testimonials -> have_posts()) : $testimonials -> the_post();
        ?>

          <li>
            
           <!-- <em class="date">24.08.2012</em> </li> -->
      <?php endwhile; ?>
      <?php wp_reset_query();?> 

        </ul>
      </div>
    </div>
  </div>
  <div class="comments-area">
    <h2>Detta säger våra kunder om oss</h2>
    <ul>
      <?php wp_reset_query();?>
      <?php
            $testimonials = new WP_Query('category_name=testimonials&&posts_per_page=4order=asc');
            while($testimonials -> have_posts()) : $testimonials -> the_post();
        ?>
      <li> <img src="<?php the_field('avatar');?>" class="alignleft">
        <div class="commet-text">
          <header class="header">
            <h3>
              <?php the_title();?>
            </h3>
            <div class="placeholder">
              <?php
                                $rating = get_field('rating'); 
                                for ($x = 0; $x < $rating; $x++) { ?>
              <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/star.jpg">
              <?php } ?>
            </div>
          </header>
          <div class="text">
            <p>
              <?php the_content();?>
            </p>
          </div>
        </div>
      </li>
      <?php endwhile; ?>
      <?php wp_reset_query();?>
    </ul>
  </div>
</div>
<?php get_footer(); ?>