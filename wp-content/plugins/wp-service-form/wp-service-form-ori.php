<?php
/*
Plugin Name: WP Service Form Plugin
Plugin URI: http://parkwaylabs.com/
Description: The form is to collect service request from customers
Version: 1.0
Author: Iyngaran Iyathurai
        iyngaran@food24.lk
        http://parkwaylabs.com/
Author URI: http://parkwaylabs.com/
*/
// user registration login form

function wp_service_form() {

    global $wp_service_form_load_css;
    $wp_service_form_load_css = true;
    $output = wp_service_form_fields();
    return $output;
}

global $wp_service_form_db_version;
$wp_service_form_db_version = '1.0';

global $table_name_services;
global $table_name_regions;
global $table_name_cities;
global $wpdb;
// the service table....
$table_name_services = $wpdb->prefix . 'wp_services';
$table_name_regions = $wpdb->prefix . 'wp_services_regions';
$table_name_cities = $wpdb->prefix . 'wp_services_cities';

// wp service form fields
function wp_service_form_fields() {
    ob_start();
    global $wpdb;
    global $table_name_services;
    global $table_name_regions;
    global $table_name_cities;
    $services_result = $wpdb->get_results("SELECT * FROM $table_name_services");
    ?>
<form name="service_form" id="service_form" action="action_page.php" method="POST"> 
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <input type="hidden" name="page_url" id="page_url" value="" />
	            <input type="text" name="service_city_data" id="service_city_data" class="form-control service_city_data" aria-label="..." placeholder="Välj Stad">
                    <div class="input-group-btn">
                        <button type="button" class="btn btn-dropdowns dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Välj tjänst <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Välj tjänst</a></li>
                            <?php foreach($services_result As $serviceIndex=>$service){?>
                            <li><a href="#<?php print($service->id);?>"><?php print($service->service_name);?></a></li>
                            <?php } ?>
                        </ul>
                    </div><!-- /btn-group -->
                    <span class="input-group-btn">
                        <button class="btn btn-info" type="submit"><i class="fa fa-search"></i></button>
                    </span>
 

    <script>
        // tell the autocomplete function to get its data from our php script
        jQuery(".service_city_data").autocomplete({
            source: "auto_load_cities.php",
            select: function (event, ui) {
                 jQuery('#page_url').val(ui.item.id);
            }
        });

    </script>
</form>
    <?php
    return ob_get_clean();
}

function wp_service_form_install()
{

    global $wp_service_form_db_version;
    global $wpdb;
    global $table_name_services;
    global $table_name_regions;
    global $table_name_cities;

    $charset_collate = $wpdb->get_charset_collate();

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    // Create the table for wp services......
        $sql = "CREATE TABLE $table_name_services (
		        id mediumint(9) NOT NULL AUTO_INCREMENT,
		        service_name varchar(200) DEFAULT '' NULL,
		        status_is SMALLINT(1) DEFAULT ' 1',
		        UNIQUE KEY id (id)
	            ) $charset_collate;";
                dbDelta($sql);

    // create the tables for regions...
        $sql = "CREATE TABLE $table_name_regions (
		        id mediumint(9) NOT NULL AUTO_INCREMENT,
		        region_name varchar(150) DEFAULT '' NULL,
		        status_is SMALLINT(1) DEFAULT ' 1',
		        UNIQUE KEY id (id)
	          ) $charset_collate;";
                dbDelta($sql);

    // create the tables for cities...
    $sql = "CREATE TABLE $table_name_cities (
		        id mediumint(9) NOT NULL AUTO_INCREMENT,
		        city_name varchar(150) DEFAULT '' NULL,
		        region_id mediumint(9) DEFAULT ' 0',
		        status_is SMALLINT(1) DEFAULT ' 1',
		        UNIQUE KEY id (id)
	          ) $charset_collate;";
                dbDelta($sql);

    add_option('wp_service_form_db_version', $wp_service_form_db_version);
}

function wp_service_form_install_data()
{
    global $wpdb;
    global $table_name_services;
    global $table_name_regions;
    global $table_name_cities;

    // -------- INSERT Services ---------------------
    $arr_services = array(
        'Accounting',
        'Customer Service'
    );

    $table_name = $table_name_services;

    foreach ($arr_services As $sIndex => $service_name) {
        $wpdb->insert(
            $table_name,
            array('service_name' => $service_name)
        );
    }

    // ------------ INSERT regions ----------------
    $arr_regions = array(
        '1'=>'Stockholms Lan',
        '2'=>'Västra Götaland',
        '3'=>'Skane Lan',
        '4'=>'Ostergotlands Lan',
        '5'=>'Uppsala Lan',
        '6'=>'Vastmanlands Lan',
        '7'=>'Jonkopings Lan',
        '8'=>'Orebro Lan',
        '9'=>'Sodermanlands Lan',
        '10'=>'Gavleborgs Lan',
        '20'=>'Hallands Lan',
        '21'=>'Vasterbottens Lan',
        '22'=>'Norrbottens Lan',
        '23'=>'Vasternorrlands Lan',
        '24'=>'Dalarnas Lan',
        '25'=>'Varmlands Lan',
        '26'=>'Kalmar Lan',
        '27'=>'Kronobergs Lan',
        '28'=>'Blekinge Lan',
        '29'=>'Jamtlands Lan',
        '30'=>'Gotlands Lan'
    );

    $table_name = $table_name_regions;

    foreach ($arr_regions As $rIndex => $region_name) {
        $wpdb->insert(
            $table_name,
            array('id'=>$rIndex,'region_name' => $region_name)
        );
    }

    // ------------- INSERT cities -----------
    $arr_cities = array(
        1=>array('Akersberga', 'Alby', 'Alta', 'Arno', 'Arsta', 'Bergshamra','Bjorko', 'Bollmora', 'Boo', 'Brevik', 'Bro', 'Bromma', 'Dalaro', 'Djursholm'),
        2=>array(),
        3=>array('Ahus', 'Anderslov', 'Angelholm', 'Arloev', 'Asmundtorp', 'Astorp', 'Barslov', 'Bastad', 'Billesholm', 'Bjarnum', 'Bjarred', 'Bjuv', 'Blentarp', 'Broby', 'Bromoella', 'Bunkeflostrand'),
        4=>array(),
        5=>array(),
        6=>array(),
        7=>array(),
        8=>array(),
        9=>array(),
        10=>array(),
        20=>array(),
        21=>array(),
        22=>array(),
        23=>array(),
        24=>array(),
        25=>array(),
        26=>array(),
        27=>array(),
        28=>array(),
        29=>array(),
        30=>array()
    );

    $table_name = $table_name_cities;

    foreach ($arr_cities As $rIndex => $arr_city_name) {
        if(count($arr_city_name)>0){
            foreach($arr_city_name as $city_name) {
                $wpdb->insert(
                    $table_name,
                    array('region_id' => $rIndex, 'city_name' => $city_name)
                );
            }
        }

    }



}

register_activation_hook( __FILE__, 'wp_service_form_install' );
register_activation_hook( __FILE__, 'wp_service_form_install_data');

// register our form css
function wp_service_form_register_css() {
    wp_register_style('wp-service-form-css', plugin_dir_url( __FILE__ ) . 'css/wp-service-forms.css');
}
add_action('init', 'wp_service_form_register_css');

// load our form css
function wp_service_form_print_css() {
    wp_print_styles('wp-service-form-css');
}
add_action('wp_footer', 'wp_service_form_print_css');

add_shortcode('wp_service_form', 'wp_service_form');