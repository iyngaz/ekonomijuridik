<?php
/**
 * Plugin Name: EJ Ajax Contact Form
 * Plugin URI: http://wilsonpartner.se
 * Description: This is a plugin that allows us to test Ajax functionality in WordPress
 * Version: 1.0.0
 * Author: Ruzly Macksood
 * Author URI: http://wilsonpartner.se
 * License: GPL2
 */

add_action('wp_enqueue_scripts', 'ej_ajax_enqueue_scripts');

function ej_ajax_enqueue_scripts()
{

    wp_enqueue_style('contact', plugins_url('/contact.css', __FILE__));
    wp_enqueue_script('contact', plugins_url('/contact.js', __FILE__));

    wp_localize_script('contact', 'ejcontact', array(
        'ajax_url' => admin_url('admin-ajax.php')
    ));

}

add_action('wp_ajax_nopriv_save_contact', 'save_contact');
add_action('wp_ajax_save_contact', 'save_contact');



function save_contact()
{    
    $channel_id = $_POST['channel_id'];
    $name = sanitize_text_field(esc_attr($_POST['name']));
    $phone = sanitize_text_field(esc_attr($_POST['phone']));
    $email = sanitize_text_field(esc_attr($_POST['email']));
    $message = sanitize_text_field(esc_attr($_POST['message']));
    $datetime = date("Y-m-d H:i:s");  

    $data = array( 'channel_id' => $channel_id,'name' => $name,'phone' => $phone,'email' => $email,'description'=>$message,'datetime'=>$datetime);
    
    $newdb = new wpdb('ekonom', 'e!k0n0m!2016', 'wilsonpartners', '159.253.24.253');    
    
    $newdb->insert('demo_lead_info', $data);
    //echo $newdb->insert_id;      
    $newdb->close();
    
    
$subject = "ekonomijuridik.se contact form";
$contact_email = "info@ekonomijuridik.se";
//$contact_email = "ahamedruzly@gmail.com";
// To send HTML mail, the Content-type header must be set.
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From:' . $contact_email. "\r\n"; // Sender's Email
$template = '<div style="padding-left:10px; color:#333;">'

. 'Namn: ' . $name . '<br/>'
. 'Telefon: ' . $phone . '<br/>'
. 'E-Post: ' . $email . '<br/>'
. 'Meddelande: ' . $message . '<br/>'
. 'Datum: ' . $datetime . '<br/>'
.'</div>';
$sendmessage = "<div style=\"background-color:#fff; color:#333;\">" . $template . "</div>";
// Message lines should not exceed 70 characters (PHP rule), so wrap it.
$sendmessage = wordwrap($sendmessage, 70);
// Send mail by PHP Mail Function.
// echo mail("ahamedruzly@gmail.com", $subject, $sendmessage, $headers);
 if(mail("info@ekonomijuridik.se", $subject, $sendmessage, $headers))
{
  echo "Mail Sent Successfully";
}else{
  echo "Mail Not Sent";
}
	
    
}





