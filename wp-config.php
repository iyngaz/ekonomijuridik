<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ekonomij_demo1');

/** MySQL database username */
define('DB_USER', 'ekonomij_demo1');

/** MySQL database password */
define('DB_PASSWORD', 'Sp]fu77n3(');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ceix1rrp0ggwy3dkziaj6zuk9w2hulloxrf7t9pawoe7pclna714eqidcj9udwze');
define('SECURE_AUTH_KEY',  'uujfadrfmki7yjgycakochveyiuzbr40fzggzhmbpihw6jmtndhsdvucdgejajno');
define('LOGGED_IN_KEY',    's0ji9cvfriyzdlp4k6qlhp47mxl95mew0k9hkw0lqccyv6dq5etqufpktvxujfvh');
define('NONCE_KEY',        '6zlxn3eausjmomfqbrgjbc86nknmcycaxsvuiidjs8y1nnclnrzh5usilvyws8fw');
define('AUTH_SALT',        'bjun4fqdwssz8hgy1ilqxuj8egm4lwhgqghfm17xw0kj9gtg6kvp5i5ao8c8wync');
define('SECURE_AUTH_SALT', 'fj86ojljsz3dzbwxi1kjx03j2yriytphkjltcjfiywjj5zotex1fa4zdo8yw6ya2');
define('LOGGED_IN_SALT',   'pvebk09hwbcgkgjzqametxxrxu2xfx7ywfzlnrgfeuv5euzmayphm9av8n8uqaoe');
define('NONCE_SALT',       'koigu0p03ivkgpzjwsa9hbeepm36jcmptsvv6j18vkc3wbjuw4ejt5xrdp70o2tv');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
