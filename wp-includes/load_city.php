<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
global $wpdb, $table_prefix;

if(!isset($wpdb))
{
    require_once('../../../wp-config.php');
    require_once('../../../wp-includes/wp-db.php');
}
$data_request = $_REQUEST['term'].'%';

$table_name_services = $wpdb->prefix . 'wp_services';
$table_name_regions = $wpdb->prefix . 'wp_services_regions';
$table_name_cities = $wpdb->prefix . 'wp_services_cities';

$sql_city_data = "SELECT city_name,(SELECT region_name FROM $table_name_regions WHERE id = region_id) as region_name FROM $table_name_cities WHERE city_name LIKE '$data_request'";
$services_result = $wpdb->get_results($sql_city_data);

$arrCityData = array();
foreach($services_result as $city){
    $data_str = $city->region_name."  =>  ".$city->city_name;
    array_push($arrCityData,$data_str);
}
# echo the json data back to the html web page
echo json_encode($arrCityData);
?>